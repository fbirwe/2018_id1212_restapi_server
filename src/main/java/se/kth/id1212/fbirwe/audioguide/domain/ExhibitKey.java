package se.kth.id1212.fbirwe.audioguide.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class ExhibitKey implements Serializable {

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, optional = false)
    private Language language;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, optional = false)
    private ExhibitStation station;

    protected ExhibitKey() {}

    public ExhibitKey(Language language, ExhibitStation station) {
        this.language = language;
        this.station = station;
    }

    public Language getLanguage() {
        return this.language;
    }

    public ExhibitStation getExhibitStation() {
        return this.station;
    }

    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        ExhibitKey other = (ExhibitKey) obj;
        if (language == null) {
            if (other.getLanguage() != null) return false;
        } else if (!language.equals(other.getLanguage())) {
            return false;
        }

        if (station == null) {
            if (other.getExhibitStation() != null) return false;
        } else if (!language.equals(other.getExhibitStation())) {
            return false;
        }

        return true;
    }
}