package se.kth.id1212.fbirwe.audioguide.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus( HttpStatus.BAD_REQUEST )
public class IdAlreadyUsedException extends RuntimeException {
    public IdAlreadyUsedException() {
        super("the mentioned id is alredy in use");
    }
}
