package se.kth.id1212.fbirwe.audioguide.domain;

import java.util.ArrayList;

public enum Filetype {
    JPG (new String[]{"image/jpeg"}, "jpg", "image"),
    MP3 (new String[]{"audio/mpeg", "audio/mp3"}, "mp3", "audio"),
    PNG (new String[]{"image/png"}, "png", "image"),
    SVG (new String[]{"image/svg+xml"}, "svg", "image");

    private final String[] mime;
    private final String extension;
    private final String type;

    Filetype( String[] mime, String extension, String type ) {
        this.mime = mime;
        this.extension = extension;
        this.type = type;
    }

    public String[] getMime() {
        return this.mime;
    }

    public String getExtension() {
        return this.extension;
    }

    public String getType() {
        return this.type;
    }
}
