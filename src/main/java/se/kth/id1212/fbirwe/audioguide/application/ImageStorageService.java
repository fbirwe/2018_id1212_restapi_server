package se.kth.id1212.fbirwe.audioguide.application;

import org.springframework.stereotype.Service;

@Service
public class ImageStorageService extends FileStorageService {
    public ImageStorageService() {
        super();
        this.target = "image";
    }
}
