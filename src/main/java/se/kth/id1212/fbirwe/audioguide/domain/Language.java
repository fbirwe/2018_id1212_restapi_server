package se.kth.id1212.fbirwe.audioguide.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "LANGUAGE")
public class Language implements LanguageDTO {
    @Id
    @NotNull
    private String shortcut;

    private String name;

    private Filetype filetype;

    protected Language() {}

    public Language( String shortcut ) {
        this.shortcut = shortcut;
    }

    public Language( String shortcut, String name ) {
        this.shortcut = shortcut;
        this.name = name;
    }

    public Language( String shortcut, String name, Filetype filetype ) {
        this( shortcut, name );
        this.filetype = filetype;
    }

    @Override
    public String getShortcut() {
        return this.shortcut;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setFiletype( Filetype filetype ) {
        this.filetype = filetype;
    }

    @Override
    public Filetype getFiletype() {
        return this.filetype;
    }

    @Override
    public String getFlag() {
        if( this.filetype != null) {
            return this.shortcut + "." + this.filetype.getExtension();
        } else {
            return "";
        }

    }

    public boolean equals(Object obj) {
        if( obj == null) {
            return false;
        }

        if( obj.getClass() != getClass() ) {
            return false;
        }

        Language other = (Language) obj;

        if( !getShortcut().equals(other.getShortcut())) {
            return false;
        }

        if( !getName().equals(other.getName()) ) {
            return false;
        }

        if( !getFlag().equals(other.getFlag()) ) {
            return false;
        }

        return true;
    }
}
