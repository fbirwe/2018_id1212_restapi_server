package se.kth.id1212.fbirwe.audioguide.domain;

public interface LanguageDTO {

    public String getShortcut();

    public String getName();

    public String getFlag();

    public Filetype getFiletype();

    public void setFiletype( Filetype filetype );

}
