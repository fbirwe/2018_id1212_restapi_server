package se.kth.id1212.fbirwe.audioguide.domain;

public interface ExhibitDTO {

    public Language getLanguage();

    public ExhibitStation getExhibitStation();

    public String getAudioDir();

    public void setDescription( String description );

    public String getDescription();

    public void setName( String name );

    public String getName();

    public Filetype getFiletype();

    public void setFiletype( Filetype filetype );

    }
