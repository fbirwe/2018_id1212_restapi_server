package se.kth.id1212.fbirwe.audioguide.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import se.kth.id1212.fbirwe.audioguide.domain.*;
import se.kth.id1212.fbirwe.audioguide.exceptions.DatasetNotFoundException;
import se.kth.id1212.fbirwe.audioguide.exceptions.IdAlreadyUsedException;
import se.kth.id1212.fbirwe.audioguide.exceptions.NotAcceptedFiletypeException;
import se.kth.id1212.fbirwe.audioguide.exceptions.VasaFileNotFoundException;
import se.kth.id1212.fbirwe.audioguide.repositories.ExhibitRepository;
import se.kth.id1212.fbirwe.audioguide.repositories.ExhibitStationRepository;
import se.kth.id1212.fbirwe.audioguide.repositories.LanguageRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
@Service
public class AudioguideManager {

    @Autowired
    private ExhibitRepository exhibitRepository;

    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private ExhibitStationRepository stationRepository;

    @Autowired
    private ImageStorageService imageStorageService;

    @Autowired
    private AudioStorageService audioStorageService;

    @Autowired
    private Environment env;

    // Language
    // CREATE
    public LanguageDTO addLanguage( String shortcut, String name, MultipartFile flagFile ) {
        languageRepository.findById( shortcut ).map(language -> {
            throw new IdAlreadyUsedException();
        }).orElseGet(() -> {
            Filetype filetype = null;

            if( flagFile != null ) {
                for ( Filetype type : Filetype.values() ) {
                    if( type.getType() == "image" && Arrays.stream( type.getMime() ).anyMatch(flagFile.getContentType()::equals) ) {
                        filetype = type;
                        break;
                    }
                }
            }

            if (filetype != null) {
                return this.languageRepository.save( new Language( shortcut, name, filetype ) );
            }
            throw new NotAcceptedFiletypeException();

        });

        LanguageDTO language = languageRepository.getOne( shortcut );

        if( flagFile != null ) {
            imageStorageService.storeFile( flagFile, language.getFlag() );
        }

        return language;
    }

    // READ
    public LanguageDTO getLanguage( String shortcut ) throws DatasetNotFoundException {
        return this.languageRepository.findById(shortcut).orElseThrow(
                () -> new DatasetNotFoundException()
        );
    }

    public BinaryDTO getLanguageImage( String shortcut ) {
        LanguageDTO language = getLanguage( shortcut );

        if( language.getFiletype() != null ) {
            Path path = imageStorageService.loadFileAsPath( language.getFlag() );

            return new BinaryDTO( path, language.getFiletype() );
        } else {
            throw new VasaFileNotFoundException();
        }
    }

    public List<LanguageDTO> getLanguages() {
        ArrayList<LanguageDTO> list = new ArrayList<>();

        for( LanguageDTO l : this.languageRepository.findAll() ) {
            list.add(l);
        }

        return list;
    }

    // UPDATE
    public LanguageDTO updateLanguage( String shortcut, String name, MultipartFile flagFile ) {
        Language language = this.languageRepository.findById(shortcut).orElseThrow(
                () -> new DatasetNotFoundException()
        );

        if ( name != null ) {
            language.setName( name );
        }

        if( flagFile != null ) {
            Filetype filetype = null;

            if( flagFile != null ) {
                for ( Filetype type : Filetype.values() ) {
                    if( type.getType() == "image" && Arrays.stream( type.getMime() ).anyMatch(flagFile.getContentType()::equals) ) {
                        filetype = type;
                        break;
                    }
                }
            }

            if ( filetype != null ) {
                language.setFiletype( filetype );

                imageStorageService.storeFile( flagFile, language.getFlag() );
            } else {
                throw new NotAcceptedFiletypeException();
            }
        }

        return this.languageRepository.save( language );
    }

    // DELETE
    public LanguageDTO deleteLanguage( String shortcut ) {
        Language language = this.languageRepository.findById(shortcut).orElseThrow(
                () -> new DatasetNotFoundException()
        );

        // delete all connected exhibits
        for( Exhibit e : this.exhibitRepository.findExhibitByLanguage( language.getShortcut() ) ) {
            this.exhibitRepository.delete( e );
        }

        this.languageRepository.delete( language );

        return language;
    }

    // Exhibit Station
    // CREATE
    public ExhibitStationDTO addExhibitStation( long id ) {
        stationRepository.findById( id ).map(language -> {
            throw new IdAlreadyUsedException();
        }).orElseGet(() -> this.stationRepository.save( new ExhibitStation( id ) ));

        return stationRepository.getOne( id );
    }

    // READ
    public ExhibitStationDTO getExhibitStation( long id ) {
        return this.stationRepository.findById( id ).orElseThrow(
                () -> new DatasetNotFoundException()
        );
    }

    public List<ExhibitStationDTO> getExhibitStations() {
        ArrayList<ExhibitStationDTO> list = new ArrayList<>();

        for( ExhibitStationDTO s : this.stationRepository.findAll() ) {
            list.add(s);
        }

        return list;
    }

    // UPDATE
    // This method is a little bit useless at the moment because the exhibit station table does only contain one column.
    // It is nevertheless implemented for future updates
    public ExhibitStationDTO updateExhibitStation( long id ) {
        ExhibitStation station = this.stationRepository.findById( id ).orElseThrow(
                () -> new DatasetNotFoundException()
        );

        return this.stationRepository.save( station );
    }

    // DELETE
    public ExhibitStationDTO deleteExhibitStation( long id ) {
        ExhibitStation station = this.stationRepository.findById( id ).orElseThrow(
                () -> new DatasetNotFoundException()
        );

        // delete all connected exhibits
        for( Exhibit e : this.exhibitRepository.findExhibitById( station.getId() ) ) {
            this.exhibitRepository.delete( e );
        }

        this.stationRepository.delete( station );

        return station;
    }

    // Exhibit
    // CREATE
    public ExhibitDTO addExhibit( String languageShortcut, long exhibitId, String name, String description, MultipartFile audioFile ) {
        // Create new language if not available
        Language language = this.languageRepository.findById( languageShortcut ).orElseGet(
                () -> this.languageRepository.save( new Language( languageShortcut ) )
        );

        // Create new exhibit if not available
        ExhibitStation station = this.stationRepository.findById( exhibitId ).orElseGet(
                () -> this.stationRepository.save( new ExhibitStation( exhibitId ) )
        );

        ExhibitKey key = getKey(languageShortcut, exhibitId);

        exhibitRepository.findById( key ).map(exhibit -> {
            throw new IdAlreadyUsedException();
        }).orElseGet(() -> {
            Filetype filetype = null;

            if( audioFile != null ) {
                for ( Filetype type : Filetype.values() ) {
                    if( type.getType() == "audio" && Arrays.stream( type.getMime() ).anyMatch(audioFile.getContentType()::equals) ) {
                        filetype = type;
                        break;
                    }
                }
            }

            if( filetype != null ) {
                return this.exhibitRepository.save( new Exhibit( language, station, filetype, name, description ) );
            }

            throw new NotAcceptedFiletypeException();
        });

        ExhibitDTO exhibit = this.exhibitRepository.getOne( key );

        if( audioFile != null ) {
            audioStorageService.storeFile( audioFile, exhibit.getAudioDir() );
        }

        return exhibit;
    }

    // READ
    public ExhibitDTO getExhibit( String languageShortcut, long exhibitId ) throws DatasetNotFoundException {
        ExhibitKey key = getKey(languageShortcut, exhibitId);

        return this.exhibitRepository.findById( key ).orElseThrow(
                DatasetNotFoundException::new
        );
    }

    public BinaryDTO getExhibitAudio( String languageShortcut, long exhibitId ) {
        ExhibitDTO exhibit = this.exhibitRepository.getOne( getKey( languageShortcut, exhibitId ) );

        if( exhibit.getFiletype() != null ) {
            Path path = audioStorageService.loadFileAsPath( exhibit.getAudioDir() );

            return new BinaryDTO( path, exhibit.getFiletype() );
        } else {
            throw new VasaFileNotFoundException();
        }
    }

    public List<ExhibitDTO> getExhibits() {
        ArrayList<ExhibitDTO> list = new ArrayList<>();

        list.addAll(this.exhibitRepository.findAll());

        return list;
    }

    public List<ExhibitDTO> getExhibitsById( long exhibitId ) {
        ArrayList<ExhibitDTO> list = new ArrayList<>();

        list.addAll(this.exhibitRepository.findExhibitById(exhibitId));

        return list;

    }

    public List<ExhibitDTO> getExhibitsByLanguage( String shortcut ) {
        ArrayList<ExhibitDTO> list = new ArrayList<>();

        for( ExhibitDTO e : this.exhibitRepository.findExhibitByLanguage( shortcut ) ) {
            list.add(e);
        }

        return list;

    }

    // UPDATE
    public ExhibitDTO updateExhibit( String shortcut, long id, String description, String name, MultipartFile file ) {
        ExhibitKey key = getKey(shortcut, id);

        Exhibit exhibit = this.exhibitRepository.findById( key ).orElseThrow(
                () -> new DatasetNotFoundException()
        );

        if( description != null ) {
            exhibit.setDescription( description );
        }

        if( name != null ) {
            exhibit.setName( name );
        }


        if( file != null ) {
            Filetype filetype = null;

            for ( Filetype type : Filetype.values() ) {
                if( type.getType() == "audio" && Arrays.stream( type.getMime() ).anyMatch(file.getContentType()::equals) ) {
                    filetype = type;
                    break;
                }
            }

            if( filetype != null ) {
                exhibit.setFiletype( filetype );

                audioStorageService.storeFile( file, exhibit.getAudioDir() );
            } else {
                throw new NotAcceptedFiletypeException();
            }
        }

        return this.exhibitRepository.save( exhibit );
    }

    // DELETE
    public ExhibitDTO deleteExhibit ( String shortcut, long id ) {
        ExhibitKey key = getKey( shortcut, id );

        Exhibit exhibit = this.exhibitRepository.findById( key ).orElseThrow(
                DatasetNotFoundException::new
        );

        this.exhibitRepository.delete( exhibit );

        return exhibit;
    }

    // Helper
    private ExhibitKey getKey( String languageShortcut, long exhibitId ) {
        Language language = this.languageRepository.findById( languageShortcut ).orElseThrow(
                DatasetNotFoundException::new
        );

        ExhibitStation station = this.stationRepository.findById( exhibitId ).orElseThrow(
                DatasetNotFoundException::new
        );

        return new ExhibitKey( language, station );
    }

    // Init
    public void init() {
        try {
            List<String> audioFiles = audioStorageService.getFiles();

            for( String s : audioFiles ) {
                String[] filenameparts = s.replaceFirst("\\..+$", "").split("_");

                // get/create station
                Long id = Long.parseLong( filenameparts[1] );

                ExhibitStation station = this.stationRepository.findById( id ).map(station1 -> station1)
                        .orElseGet(() -> this.stationRepository.save( new ExhibitStation( id ) ) );

                // get/create language
                Language language = this.languageRepository.findById(filenameparts[0]).map(language1 -> language1)
                        .orElseGet(() -> this.languageRepository.save( new Language( filenameparts[0]) ) );

                // include image if available
                if( language.getFiletype() == null ) {
                    for( String imageFile : imageStorageService.getFiles() ) {
                        String[] imageFilenameparts = s.replaceFirst("\\..+$", "").split("_");

                        if( imageFilenameparts[0].equals(language.getShortcut()) ) {
                            language.setFiletype( imageStorageService.getFiletype( imageFile ) );
                            this.languageRepository.save( language );
                            break;
                        }
                    }
                }

                // get Filetype
                Filetype audioFiletype = audioStorageService.getFiletype( s );

                // get/create exhibit
                this.exhibitRepository.save( new Exhibit( language,  station, audioFiletype) );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
