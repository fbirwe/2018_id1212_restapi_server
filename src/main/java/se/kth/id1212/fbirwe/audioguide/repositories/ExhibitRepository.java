package se.kth.id1212.fbirwe.audioguide.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import se.kth.id1212.fbirwe.audioguide.domain.Exhibit;
import se.kth.id1212.fbirwe.audioguide.domain.ExhibitKey;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface ExhibitRepository extends JpaRepository<Exhibit, ExhibitKey> {

    @Query("SELECT e FROM Exhibit e where e.key.station.id = :id")
    List<Exhibit> findExhibitById(@Param("id") Long id);

    @Query("SELECT e FROM Exhibit e where e.key.language.shortcut = :shortcut")
    List<Exhibit> findExhibitByLanguage(@Param("shortcut") String shortcut);
}