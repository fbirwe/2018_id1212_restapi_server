package se.kth.id1212.fbirwe.audioguide.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import se.kth.id1212.fbirwe.audioguide.domain.Filetype;
import se.kth.id1212.fbirwe.audioguide.exceptions.FileStorageException;
import se.kth.id1212.fbirwe.audioguide.exceptions.VasaFileNotFoundException;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class FileStorageService {

    @Autowired
    private Environment env;

    private Path fileStorageLocation;
    protected String target;

    @PostConstruct
    public void init(){
        this.fileStorageLocation = Paths.get( env.getProperty("data_dir") ).toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);

            if( target != null ) {
                this.fileStorageLocation = Paths.get( env.getProperty("data_dir"), target ).toAbsolutePath().normalize();
                Files.createDirectories(this.fileStorageLocation);
            }
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }

    }


    public void storeFile(MultipartFile file, String filename) {

        try {
            // Check if the file's name contains invalid characters
            if(filename.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + filename);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(filename);

            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + filename + ". Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(String filename) {
        try {
            return new UrlResource( loadFileAsPath(filename).toUri() );
        } catch (MalformedURLException ex) {
            throw new VasaFileNotFoundException("File not found " + filename, ex);
        }
    }

    public Path loadFileAsPath(String filename) {
        try {
            Path filePath = this.fileStorageLocation.resolve(filename).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return filePath;
            } else {
                throw new VasaFileNotFoundException("File not found " + filename);
            }
        } catch (MalformedURLException ex) {
            throw new VasaFileNotFoundException("File not found " + filename, ex);
        }
    }

    public List<String> getFiles() throws IOException {

        List<String> fileNames = new ArrayList<>();

        DirectoryStream<Path> directoryStream = Files.newDirectoryStream( this.fileStorageLocation  );

        for (Path path : directoryStream) {
                fileNames.add(path.getFileName().toString());
        }

        return fileNames;
    }

    public Filetype getFiletype( String filename ) throws IOException {
        System.out.println( );

        Path file = loadFileAsPath( filename );
        String extension = com.google.common.io.Files.getFileExtension( filename );

        for ( Filetype type : Filetype.values() ) {
            if(type.getExtension().equals(extension)) {
                return type;
            }
        }

        return null;

    }

    public boolean fileExists( String filename ) {
        return new File(String.valueOf(this.fileStorageLocation.resolve(filename).toAbsolutePath())).exists();
    }

}
