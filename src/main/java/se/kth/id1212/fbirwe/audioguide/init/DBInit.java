package se.kth.id1212.fbirwe.audioguide.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import se.kth.id1212.fbirwe.audioguide.application.AudioguideManager;

import java.io.File;

@Component
public class DBInit implements ApplicationListener<ApplicationReadyEvent> {
    /**
     * This event is executed as late as conceivably possible to indicate that
     * the application is ready to service requests.
     */

    @Autowired
    private AudioguideManager audioguideManager;


    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {

        audioguideManager.init();

        /*
        ClassLoader classLoader = getClass().getClassLoader();

        File folder = new File(classLoader.getResource("flags").getFile());

        //File is found
        System.out.println("File Found : " + folder.exists());

        for (final File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory()) {
                audioguideManager.addFlag( fileEntry.getName() );
            }
        }
        */
    }

}
