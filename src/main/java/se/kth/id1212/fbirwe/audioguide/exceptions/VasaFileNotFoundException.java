package se.kth.id1212.fbirwe.audioguide.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class VasaFileNotFoundException extends RuntimeException {
    public VasaFileNotFoundException() {
        super("The requested file could not be found");
    }

    public VasaFileNotFoundException(String message) {
        super(message);
    }

    public VasaFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
