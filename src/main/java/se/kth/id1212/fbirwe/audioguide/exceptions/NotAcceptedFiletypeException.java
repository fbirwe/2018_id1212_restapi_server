package se.kth.id1212.fbirwe.audioguide.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class NotAcceptedFiletypeException extends RuntimeException {
    public NotAcceptedFiletypeException() {
        super("The file does not have an accepted filetype");
    }

    public NotAcceptedFiletypeException(String message) {
        super(message);
    }

    public NotAcceptedFiletypeException(String message, Throwable cause) {
        super(message, cause);
    }
}
