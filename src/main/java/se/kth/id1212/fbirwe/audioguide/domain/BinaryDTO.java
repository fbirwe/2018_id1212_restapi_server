package se.kth.id1212.fbirwe.audioguide.domain;

import java.nio.file.Path;

public class BinaryDTO {
    private Path path;
    private Filetype filetype;

    public BinaryDTO( Path path, Filetype filetype ) {
        this.path = path;
        this.filetype = filetype;
    }

    public Path getPath() {
        return this.path;
    }

    public Filetype getFiletype() {
        return this.filetype;
    }
}
