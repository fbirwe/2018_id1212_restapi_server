package se.kth.id1212.fbirwe.audioguide.domain;

public interface ExhibitStationDTO {

    public long getId();

    public void setId( long id );

}
