package se.kth.id1212.fbirwe.audioguide.rest;

import groovy.lang.Lazy;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import se.kth.id1212.fbirwe.audioguide.application.AudioStorageService;
import se.kth.id1212.fbirwe.audioguide.application.AudioguideManager;
import se.kth.id1212.fbirwe.audioguide.application.FileStorageService;
import se.kth.id1212.fbirwe.audioguide.application.ImageStorageService;
import se.kth.id1212.fbirwe.audioguide.domain.BinaryDTO;
import se.kth.id1212.fbirwe.audioguide.domain.ExhibitDTO;
import se.kth.id1212.fbirwe.audioguide.domain.LanguageDTO;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.158.25:3000"}, maxAge = 3600)
@RestController
public class AudioguideController {

    @Autowired
    private Environment env;

    @Autowired
    private AudioguideManager manager;

    @Autowired
    private AudioStorageService audioStorageService;


    // Languages
    // CREATE / POST
    @RequestMapping(value = "/language/{language}/name/{name}", method = RequestMethod.POST)
    public LanguageDTO addLanguageWithFlag(@PathVariable("language") String language, @PathVariable("name") String name, @RequestParam( name = "file", required = false) MultipartFile file ) {
        return manager.addLanguage(language, name, file);
    }

    // READ / GET
    @RequestMapping(value = "/language", method = RequestMethod.GET)
    public List<LanguageDTO> allLanguages() {
        return manager.getLanguages();
    }

    @RequestMapping(value = "/language/{language}", method = RequestMethod.GET)
    public LanguageDTO getLanguage(@PathVariable("language") String language) {
        return manager.getLanguage(language);
    }

    @RequestMapping(value = "/language/{language}/image", method = RequestMethod.GET)
    public void getLanguageImage(@PathVariable("language") String shortcut, HttpServletResponse response) throws IOException {
        BinaryDTO image = manager.getLanguageImage( shortcut );


        response.setContentType( image.getFiletype().getMime()[0] );

        response.setContentLength((int) Files.size( image.getPath() ));
        Files.copy( image.getPath(), response.getOutputStream());
        response.flushBuffer();
    }



    // UPDATE / PUT
    @RequestMapping(value = "/language/{language}/name/{name}", method = RequestMethod.PUT)
    public LanguageDTO updateLanguage(@PathVariable("language") String language, @PathVariable("name") String name, @RequestParam( name = "file", required = false) MultipartFile file ) {
        return manager.updateLanguage( language, name, file);
    }

    // DELETE
    @RequestMapping(value = "/language/{language}", method = RequestMethod.DELETE)
    public LanguageDTO deleteLanguage(@PathVariable("language") String language) {
        return manager.deleteLanguage( language );
    }

    // Exhibit
    // CREATE / POST
    @RequestMapping(value = "/exhibit/station/{exhibitId}/language/{language}", method = RequestMethod.POST)
    public ExhibitDTO addExhibit(@PathVariable("exhibitId") long exhibitId, @PathVariable("language") String language, @RequestParam(name = "file", required = false) MultipartFile file, @RequestParam(name = "description", required = false) String description, @RequestParam(name = "name", required = false) String name, HttpServletResponse response) throws IOException {
        return manager.addExhibit(language, exhibitId, name, description, file);
    }

    // READ / GET
    @RequestMapping(value = "/exhibit", method = RequestMethod.GET)
    public List<ExhibitDTO> allExhibits() {
        return manager.getExhibits();
    }

    @RequestMapping(value = "/exhibit/station/{exhibitId}/language/{language}", method = RequestMethod.GET)
    public ExhibitDTO getExhibit(@PathVariable("exhibitId") long exhibitId, @PathVariable("language") String language) {
        return manager.getExhibit( language, exhibitId );
    }

    @RequestMapping(value = "/exhibit/station/{exhibitId}", method = RequestMethod.GET)
    public List<ExhibitDTO> getExhibitsById(@PathVariable("exhibitId") long exhibitId) {
        return manager.getExhibitsById( exhibitId );
    }

    @RequestMapping(value = "/exhibit/language/{language}", method = RequestMethod.GET)
    public List<ExhibitDTO> getExhibitsByLanguage(@PathVariable("language") String shortcut) {
        return manager.getExhibitsByLanguage( shortcut );
    }

    @RequestMapping(value = "/exhibit/station/{exhibitId}/language/{language}/audio", method = RequestMethod.GET)
    public void getExhibitAudio(@PathVariable("exhibitId") long exhibitId, @PathVariable("language") String language, HttpServletResponse response) throws IOException {
        BinaryDTO audio = manager.getExhibitAudio( language, exhibitId );

        response.setContentType( audio.getFiletype().getMime()[0] );
        response.setContentLength((int) Files.size( audio.getPath() ));
        Files.copy( audio.getPath(), response.getOutputStream());
        response.flushBuffer();
    }

    // UPDATE / PUT
    @RequestMapping(value = "/exhibit/station/{exhibitId}/language/{language}", method = RequestMethod.PUT)
    public ExhibitDTO updateExhibit(@PathVariable("exhibitId") long exhibitId, @PathVariable("language") String language, @RequestParam(name = "file", required = false) MultipartFile file, @RequestParam(name = "description", required = false) String description, @RequestParam(name = "name", required = false) String name) {
        return manager.updateExhibit( language, exhibitId, description, name, file);
    }

    // DELETE
    @RequestMapping(value = "/exhibit/station/{exhibitId}/language/{language}", method = RequestMethod.DELETE)
    public ExhibitDTO deleteExhibit(@PathVariable("exhibitId") long exhibitId, @PathVariable("language") String language) {
        return manager.deleteExhibit( language, exhibitId );
    }
}
