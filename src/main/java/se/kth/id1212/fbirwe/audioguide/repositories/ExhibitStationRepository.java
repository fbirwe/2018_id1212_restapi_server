package se.kth.id1212.fbirwe.audioguide.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import se.kth.id1212.fbirwe.audioguide.domain.ExhibitStation;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface ExhibitStationRepository extends JpaRepository<ExhibitStation, Long> {
}
