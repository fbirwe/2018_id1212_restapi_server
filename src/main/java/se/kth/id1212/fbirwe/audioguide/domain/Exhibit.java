package se.kth.id1212.fbirwe.audioguide.domain;

import org.springframework.data.annotation.Id;

import javax.persistence.*;

@Entity
@Table(name = "EXHIBIT")
public class Exhibit implements ExhibitDTO {
    private static final String SEQUENCE_NAME_KEY = "SEQ_NAME";

    @EmbeddedId
    private ExhibitKey key;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "FILETYPE")
    private Filetype filetype;

    @Column(name = "NAME")
    private String name;

    protected Exhibit() {}

    public Exhibit( Language language, ExhibitStation station, Filetype filetype ) {
        this.key = new ExhibitKey( language, station );
        this.filetype = filetype;
    }

    public Exhibit( Language language, ExhibitStation station, Filetype filetype, String name, String description ) {
        this( language, station, filetype );
        this.description = description;
        this.name = name;
    }

    @Override
    public Language getLanguage() {
        return this.key.getLanguage();
    }

    @Override
    public Filetype getFiletype() {
        return this.filetype;
    }

    @Override
    public void setFiletype(Filetype filetype) {
        this.filetype = filetype;
    }

    @Override
    public String getName() { return this.name; }

    @Override
    public ExhibitStation getExhibitStation() {
        return this.key.getExhibitStation();
    }

    @Override
    public String getAudioDir() {
        if(this.filetype != null) {
            String extension = getFiletype().getExtension();

            return this.getLanguage().getShortcut() + "_" + this.getExhibitStation().getId() + "." + extension;
        } else {
            return "";
        }

    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
