package se.kth.id1212.fbirwe.audioguide.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "EXHIBIT_STATION")
public class ExhibitStation implements ExhibitStationDTO {

    @Id
    @NotNull
    private long id;

    protected ExhibitStation() {}

    public ExhibitStation(long id) {
        this.id = id;
    }

    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }
}
