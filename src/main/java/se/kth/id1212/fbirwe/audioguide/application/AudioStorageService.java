package se.kth.id1212.fbirwe.audioguide.application;

import org.springframework.stereotype.Service;

@Service
public class AudioStorageService extends FileStorageService {
    public AudioStorageService() {
        super();
        this.target = "audio";
    }
}
